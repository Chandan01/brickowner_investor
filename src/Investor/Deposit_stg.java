package Investor;


import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Deposit_stg {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://staging.brickowner.com/";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testUntitled() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.cssSelector("#menu-item-172 > a")).click();
    Thread.sleep(1000);
    driver.findElement(By.id("emailId")).clear();
    Thread.sleep(1000);
    driver.findElement(By.id("emailId")).sendKeys("deposite@mailinator.com");
    Thread.sleep(1000);
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("test@123");
  //  driver.findElement(By.id("//input[@id='checkbox']")).click();
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("input.sbmit-btn.bo-submit-btn")).click();
    
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("ul.bo-prop-menu2 > li > a")).click();
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("ul.bo-prop-menu2 > li > a")).click();
    Thread.sleep(2000);
    driver.findElement(By.linkText("Fund Account")).click();
    Thread.sleep(3000);
    driver.findElement(By.id("fundAmount")).clear();
    Thread.sleep(3000);
    driver.findElement(By.id("fundAmount")).sendKeys("10");
    Thread.sleep(3000);
    driver.findElement(By.id("cardHolderName")).clear();
    Thread.sleep(3000);
    driver.findElement(By.id("cardHolderName")).sendKeys("test");
    Thread.sleep(3000);
    driver.findElement(By.id("cardNumber")).clear();
    Thread.sleep(3000);
    driver.findElement(By.id("cardNumber")).sendKeys("3569990000000132");
    Thread.sleep(3000);
    driver.findElement(By.id("cvv")).clear();
    Thread.sleep(3000);
    driver.findElement(By.id("cvv")).sendKeys("123");

 
    driver.findElement(By.xpath("//input[@id='save_card']//following-sibling::label")).click();
    driver.findElement(By.xpath(".//*[@id='kyc-stp-two']/div[3]/my-kyc-step-three/div/div/section/div[1]/form/button")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("code")).sendKeys("secret3");
	driver.findElement(By.name("submit")).click();
	Thread.sleep(3000);
	String expected= "Congratulations.";
	Assert.assertEquals(driver.findElement(By.xpath("//*[contains(text(),'Congratulations.')]")).getText(), expected);
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
  driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
