package Investor;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class Activation_kyc_stg {

	WebDriver driver;
	String base_url;
   String Email= "kyc15@mailinator.com";

	@Parameters("browserType")		
	@BeforeMethod
	public void beforeMethod(String browser) {

		if (browser.equalsIgnoreCase("Firefox"))
		{
			driver= new FirefoxDriver();

		}else if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", "/home/user/Documents/chromedriver");

			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		base_url= "https://devapp.brickowner.com/login";
		driver.get(base_url);
		
	}



	@Test
	public void Happy_path_Registartion() throws InterruptedException {
		
		driver.findElement(By.partialLinkText("Join")).click();

		driver.findElement(By.id("firstname")).sendKeys("Selenium");
		driver.findElement(By.id("lastname")).sendKeys("Lastname");
		driver.findElement(By.id("emailId")).sendKeys(Email);
		driver.findElement(By.id("password")).sendKeys("test@123");
		driver.findElement(By.cssSelector(".sbmit-btn.bo-submit-btn")).click();


		String Actual = "Thank you for registering, we have sent you a confirmation link";

		String  Expected= driver.findElement(By.xpath("//div[contains(text(),'Thank you for registering, we have sent you a confirmation link')]")).getText();

		Assert.assertEquals(Actual, Expected);

		driver.get("https://www.mailinator.com");
		driver.findElement(By.id("inboxfield")).sendKeys(Email);
		driver.findElement(By.cssSelector(".btn.btn-dark")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[contains(text(),'THIS IS FOR DEV PURPOSE ONLY!!Brickowner account activation link')]")).click();

		Thread.sleep(1000);


		driver.switchTo().frame("publicshowmaildivcontent");

		driver.findElement(By.xpath("//img[@alt='Confirm your account']")).click();

        Thread.sleep(8000);
        
	    	driver.switchTo().window("https://devapp.brickowner.com/dashboard");
	 
		//driver.findElement(By.linkText("Fund Account")).click();
        driver.findElement(By.xpath("//*[contains(text(),'Fund Account')]")).click();
		driver.findElement(By.xpath("//*[contains(text(),'Everyday Investor')]")).click();
		driver.findElement(By.xpath("//input[@id='yes1']//following-sibling::label")).click();

			
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='yes2']//following-sibling::label")).click();
			 
			
		
		Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='yes3']//following-sibling::label")).click();
		
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@id='yes4']//following-sibling::label")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@id='yes5']//following-sibling::label")).click();
		Thread.sleep(5000);
		WebElement we= driver.findElement(By.xpath(".//*[@id='kyc-stp-two']/my-kyc-step-one/div/div/fieldset/form/button"));
		if(we.isEnabled()){	
			we.click();
		}
		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@id='thing']//following-sibling::label")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='kyc-stp-two']/my-kyc-step-one/div/div/div/form/button")).click();

		//Date selection

		Thread.sleep(3000);
		Select date = new Select(driver.findElement(By.name("dateOfBirthDay")));

		date.selectByIndex(4);

		Select month = new Select(driver.findElement(By.name("dateOfBirthMonth")));

		month.selectByValue("string:06");

		Select year = new Select(driver.findElement(By.name("dateOfBirthYear")));

		year.selectByVisibleText("1970");

		driver.findElement(By.name("postcode")).sendKeys("BA133BN");
		driver.findElement(By.xpath("//a[contains(text(),'Click here to enter it manually')]")).click();

		driver.findElement(By.name("addressLine1")).sendKeys("william street");

		driver.findElement(By.name("city")).sendKeys("london");

		Thread.sleep(1000);

		Select country = new Select(driver.findElement(By.name("country")));

		country.selectByVisibleText("Afghanistan");
		Select nationality = new Select(driver.findElement(By.name("nationality")));

		nationality.selectByVisibleText("Australian");
		driver.findElement(By.xpath(".//*[@id='kyc-stp-two']/div[2]/my-kyc-step-two/div/div/form/button")).click();
		Thread.sleep(2000);
		//Fund Your Account 
		driver.findElement(By.id("fundAmount")).sendKeys("100");
		Thread.sleep(1000);
		driver.findElement(By.id("cardHolderName")).sendKeys("Jon Shan");
		driver.findElement(By.id("cardNumber")).sendKeys("3569990000000157");
		Thread.sleep(2000);
		Select ex_month = new Select(driver.findElement(By.name("expiryDateMonth")));

		ex_month.selectByVisibleText("May");

		Select ex_year = new Select(driver.findElement(By.name("expiryDateYear")));

		ex_year.selectByVisibleText("2017");

		driver.findElement(By.id("cvv")).sendKeys("123");
		driver.findElement(By.xpath("//input[@id='save_card']//following-sibling::label")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='kyc_disclaimer']//following-sibling::label")).click();
		driver.findElement(By.xpath(".//*[@id='kyc-stp-two']/div[3]/my-kyc-step-three/div/div/section/div[1]/form/button")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("code")).sendKeys("secret3");
		driver.findElement(By.name("submit")).click();
		Thread.sleep(3000);
		String expected= "Congratulations.";
		Assert.assertEquals(driver.findElement(By.xpath("//*[contains(text(),'Congratulations.')]")).getText(), expected);

	}




	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
