package Investor;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.*;


import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Registration {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://dev.brickowner.com/";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testUntitled() throws Exception {
	driver.get(baseUrl);
	  
       
    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
    driver.get("https://stagingapp.brickowner.com/register");
    
 // Registration Blank
    driver.findElement(By.id("checkbox")).click();
    driver.findElement(By.cssSelector("input.sbmit-btn.bo-submit-btn")).click();
    
    Assert.assertEquals("Please enter your first name here", driver.findElement(By.xpath("//*[contains(text(),'Please enter your first name here')]")).getText());
    Assert.assertEquals("Please enter your last name here", driver.findElement(By.xpath("//*[contains(text(),'Please enter your last name here')]")).getText());
    Assert.assertEquals("Please enter your email id here", driver.findElement(By.xpath("//*[contains(text(),'Please enter your email id here')]")).getText());
    Assert.assertEquals("Please enter your password here", driver.findElement(By.xpath("//*[contains(text(),'Please enter your password here')]")).getText());
    
    // Registration Existing
    driver.findElement(By.id("firstname")).clear();
    driver.findElement(By.id("firstname")).sendKeys("test");
    Thread.sleep(2000);
    driver.findElement(By.id("lastname")).clear();	
    driver.findElement(By.id("lastname")).sendKeys("test");
    Thread.sleep(2000);
    driver.findElement(By.id("emailId")).clear();
    driver.findElement(By.id("emailId")).sendKeys("teststg1@mailinator.com");
    Thread.sleep(2000);
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("Inno@123");
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("input.sbmit-btn.bo-submit-btn")).click();
    Thread.sleep(2000);
    
    Assert.assertEquals("Email already exist", driver.findElement(By.xpath("//*[contains(text(),'Email already exist')]")).getText());
    
//Weak password
    
    driver.findElement(By.id("firstname")).clear();
    driver.findElement(By.id("firstname")).sendKeys("shruti");
    driver.findElement(By.id("lastname")).clear();
    driver.findElement(By.id("lastname")).sendKeys("test");
    driver.findElement(By.id("emailId")).clear();
    driver.findElement(By.id("emailId")).sendKeys("sg@mailinator.com");
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("testbrick");
    driver.findElement(By.cssSelector("input.sbmit-btn.bo-submit-btn")).click();
   
     Assert.assertEquals("This password is weak", driver.findElement(By.xpath("//*[contains(text(),'This password is weak')]")).getText());
    
    
  
     // For Invalid values 
     driver.findElement(By.id("firstname")).clear();
     driver.findElement(By.id("firstname")).sendKeys("$$$");
     Assert.assertFalse(driver.findElement(By.xpath("//*[@id='firstname']")).getText().matches("^[a-z0-9_-]{3,15}$"));
     driver.findElement(By.id("lastname")).clear();
     driver.findElement(By.id("lastname")).sendKeys("$%DD  $%$%");
     Assert.assertFalse(driver.findElement(By.xpath("//*[@id='lastname']")).getText().matches("^[a-z0-9_-]{3,15}$"));
     driver.findElement(By.id("emailId")).clear();
     driver.findElement(By.id("emailId")).sendKeys("xyz@msdm");
     Assert.assertFalse(driver.findElement(By.xpath("//*[@id='emailId']")).getText().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"));
     driver.findElement(By.id("password")).clear();
     driver.findElement(By.id("password")).sendKeys("1*@@3");
     Assert.assertFalse(Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})").matcher(driver.findElement(By.xpath("//*[@id='password']")).getText()).find());
   //  new Actions(driver).moveToElement(driver.findElement(By.xpath("//html/body/div[1]/div[3]/div[1]/div/div/form/div[4]/label[3]"))).click().perform();
     // ERROR: Caught exception [ERROR: Unsupported command [clickAt | css=input.sbmit-btn.bo-submit-btn | ]]
     Assert.assertEquals("Please enter valid first name (characters)", driver.findElement(By.xpath("//*[contains(text(),'Please enter valid first name (characters)')]")).getText());
     Assert.assertEquals("Please enter valid last name (characters)", driver.findElement(By.xpath("//*[contains(text(),'Please enter valid last name (characters)')]")).getText());
     Assert.assertEquals("Please enter valid password (min. 8 characters)", driver.findElement(By.xpath("//*[contains(text(),'Please enter valid password (min. 8 characters)')]")).getText());
     driver.findElement(By.cssSelector("input.sbmit-btn.bo-submit-btn")).click();
    //Registration New
    driver.findElement(By.id("firstname")).clear();
    driver.findElement(By.id("firstname")).sendKeys("test selenium");
    Thread.sleep(2000);
    driver.findElement(By.id("lastname")).clear();
    driver.findElement(By.id("lastname")).sendKeys("test automation");
    Thread.sleep(2000);
    driver.findElement(By.id("emailId")).clear();
    driver.findElement(By.id("emailId")).sendKeys("teststgsh@mailinator.com");
    Thread.sleep(2000);
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("Inno@123");
    Thread.sleep(5000);
    driver.findElement(By.id("checkbox")).click();
    driver.findElement(By.cssSelector("input.sbmit-btn.bo-submit-btn")).click();
    Thread.sleep(2000);
    
    
    

  }
  @AfterClass
  public void tearDown() throws Exception {
   driver.quit();
    String verificationErrorString = verificationErrors.toString();
    
  }
}
