package Investor;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class Header_Footer {
	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {

		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://dev.brickowner.com/");

	}


	@Test
	public void f() throws InterruptedException {
		 String handle= driver.getWindowHandle();
		//Header links
		driver.findElement(By.linkText("Why Brickowner")).click();
		Assert.assertEquals("Why Brickowner", driver.findElement(By.cssSelector(".spb-asset-content>h1")).getText());

		driver.findElement(By.linkText("How it works")).click();
		Assert.assertEquals("How it works", driver.findElement(By.cssSelector(".spb-asset-content>h1")).getText());


		driver.findElement(By.linkText("Discover properties")).click();
		Assert.assertEquals("Discover properties", driver.findElement(By.cssSelector(".bo-main-prop-tit>h1")).getText());

		driver.findElement(By.linkText("Insights")).click();
		Assert.assertEquals("Insights", driver.findElement(By.cssSelector(".spb-asset-content>h1")).getText());
		//Footer links

		driver.findElement(By.linkText("About Us")).click();
		Assert.assertEquals("Why Brickowner", driver.findElement(By.cssSelector(".spb-asset-content>h1")).getText());



		driver.findElement(By.linkText("Contact")).click();
		Assert.assertEquals("Contact", driver.findElement(By.cssSelector(".spb-asset-content>h1")).getText());
		driver.findElement(By.linkText("Key Risks")).click();

		Assert.assertEquals("Key Risks", driver.findElement(By.xpath(".//*[@id='354']/section[1]/div/div/section[2]/div/div/div/h1")).getText());

		driver.findElement(By.linkText("FAQs")).click();

		Assert.assertEquals("FAQs", driver.findElement(By.xpath(".//*[@id='main-container']/div[4]/div/div/section[1]/div/div/section[2]/div/div/div/h1")).getText());


		driver.findElement(By.linkText("Privacy & Cookie Policy")).click();
		Assert.assertEquals("Privacy & Cookie Policy", driver.findElement(By.xpath("//*[@id='358']/section[1]/div/div/section[2]/div/div/div/h1")).getText());

		driver.findElement(By.linkText("Terms & Conditions")).click();

		Assert.assertEquals("Terms & Conditions", driver.findElement(By.xpath("//*[@id='274']/section[1]/div/div/section[2]/div/div/div/h1")).getText());

		//Social Media links
		 // Facebook
	    driver.findElement(By.xpath("//img[@src='https://dopg0v92zqygj.cloudfront.net/wp-content/uploads/2016/05/02080052/fb.png']")).click();
	
	  
	   for (String handle1 : driver.getWindowHandles()) {
	    	 
	    	//System.out.println(handle1);

	    	driver.switchTo().window(handle1);
	   }
	 Thread.sleep(2000);
	  if (driver.findElements(By.linkText("Facebook")).size() != 0){
		    driver.close();
	  }
	
	    
	   
	    driver.switchTo().window(handle);
	    Assert.assertEquals("Terms & Conditions", driver.findElement(By.linkText("Terms & Conditions")).getText());
	   
	    //twitter
	    
	    driver.findElement(By.xpath("//img[@src='https://dopg0v92zqygj.cloudfront.net/wp-content/uploads/2016/05/02080059/twt.png']")).click();
	    
	    for (String handle2 : driver.getWindowHandles()) {
	     	 
	     	//System.out.println(handle2);

	     	driver.switchTo().window(handle2);
	    }

	    Thread.sleep(2000);
	    if (driver.findElements(By.id("search-query")).size() != 0){
		    driver.close();
	  }
	  
	    
	     driver.switchTo().window(handle);
	     
	     Assert.assertEquals("Terms & Conditions", driver.findElement(By.linkText("Terms & Conditions")).getText());
	    
	   //google+
	     
	     driver.findElement(By.xpath("//*[@id='text-2']/div/ul/li[3]/a/img")).click();
	     
	     for (String handle3 : driver.getWindowHandles()) {
	      	 
	   //   	System.out.println(handle3);

	      	driver.switchTo().window(handle3);
	     }
	     Thread.sleep(2000);
	      
	     if (driver.findElements(By.xpath(".//*[@id='gbl']/span")).size() != 0){
			    driver.close();
		  }
		  
	      driver.switchTo().window(handle);
	      
	      Assert.assertEquals("Terms & Conditions", driver.findElement(By.linkText("Terms & Conditions")).getText());
	      //Linkedin
	      
	      driver.findElement(By.xpath(".//*[@id='text-2']/div/ul/li[4]/a/img")).click();
	      
	      for (String handle4 : driver.getWindowHandles()) {
	       	 
	     //  	System.out.println(handle4);

	       	driver.switchTo().window(handle4);
	      }

	      Thread.sleep(2000);
	      
		     if (driver.findElements(By.id(".//*[@id='application-body']")).size() != 0){
				    driver.close();
			  }
			  
	       driver.switchTo().window(handle);
	       
	       Assert.assertEquals("Terms & Conditions", driver.findElement(By.linkText("Terms & Conditions")).getText());
	   
	}

	



	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
