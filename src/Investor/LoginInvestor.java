package Investor;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.*;


import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
//valid Login
public class LoginInvestor {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://devapp.brickowner.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testLogin() throws Exception {
    driver.get(baseUrl + "/login");
    driver.findElement(By.xpath("//input[@value='Sign in']")).click();
    
    Assert.assertEquals("Please enter your email id here", driver.findElement(By.xpath("//*[contains(text(),'Please enter your email id here')]")).getText());
    
    Assert.assertEquals("Please enter your password here", driver.findElement(By.xpath("//*[contains(text(),'Please enter your password here')]")).getText());
    
    driver.findElement(By.id("emailId")).clear();
    driver.findElement(By.id("emailId")).sendKeys("kycs@mailinator.com");
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("test@123");
    
    driver.findElement(By.xpath("//input[@value='Sign in']")).click();
    Thread.sleep(1000);
    
    Assert.assertEquals("Dashboard", driver.findElement(By.linkText("Dashboard")).getText());
    
  }

  @AfterClass
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    
  }

 
}
